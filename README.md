# Inspectlet

This module will help non-developers integrate Drupal with Inspectlet.com
Inspectlet is a usability tool which can generate heatmaps and clickmaps.
It also records where the mouse is on the screen, so that site owners can become
aware which buttons and / or links people are hovering over but don't click.
For more information see: http://www.inspectlet.com/

## Installation:

- Enable the module.
- Go to [/admin/config/system/inspectlet]().
- Paste your Inspectlet code in there.
- Wait for your data to come in.
